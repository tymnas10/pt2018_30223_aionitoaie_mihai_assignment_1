package PT2018.demo.DemoProject;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

public class GUI {
	
		JFrame window = new JFrame("Polinoms");
		JTextField _Field1 = new JTextField(),_Field2 = new JTextField(), _Result = new JTextField(), _Rest = new JTextField();
		JLabel _Name1 = new JLabel("Polinom 1"),_Name2 = new JLabel("Polinom 2"), ResultL = new JLabel("Rezultat"), RestL = new JLabel("Rest");
		Font normalFont = new Font("Times New Roman", Font.BOLD, 16);
		JButton add = new JButton("+"),sub = new JButton("-"),deriv = new JButton("d()/dx"),integr = new JButton("integrate"),mul = new JButton("*"),div = new JButton("/");
		
		AddButtonHandler addHandler = new AddButtonHandler();
		SubButtonHandler subHandler = new SubButtonHandler();
		DerivButtonHandler derivHandler = new DerivButtonHandler();
		IntegrButtonHandler integrHandler = new IntegrButtonHandler();
		MulButtonHandler mulHandler = new MulButtonHandler();
		DivButtonHandler divHandler = new DivButtonHandler();
				
		Container con;
		Polinom p1;
		Polinom p2;
		Polinom p3 = new Polinom();
		
	
		public GUI() {
			
			window.setSize(700,500);
			window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			window.getContentPane().setBackground(Color.GRAY);
			window.setLayout(null);
			
			con = window.getContentPane();
			
			//citire polinoame
			_Name1.setFont(normalFont);
			_Name2.setFont(normalFont);
			ResultL.setFont(normalFont);
			RestL.setFont(normalFont);
			_Name1.setForeground(Color.black);
			_Name2.setForeground(Color.black);
			ResultL.setForeground(Color.black);
			RestL.setForeground(Color.black);
			_Name1.setBounds(30,30,100,30);
			_Name2.setBounds(30,80,100,30);
			ResultL.setBounds(30,170,100,30);
			RestL.setBounds(30,270,170,40);
			con.add(_Name1);
			con.add(_Name2);
			con.add(RestL);
			con.add(ResultL);
			
			
			_Field1.setBounds(30,55,170,30);
			_Field1.setFont(normalFont);
			_Field2.setBounds(30,105,170,30);
			_Field2.setFont(normalFont);
			_Result.setBounds(30,200,350,40);
			_Result.setEditable(false);
			_Rest.setBounds(30,300,350,40);
			_Rest.setEditable(false);
			con.add(_Rest);
			con.add(_Result);
			con.add(_Field1);
			con.add(_Field2);
			
			
			//operatii
			add.setBounds(300,40,50,30);
			add.addActionListener(addHandler);
			sub.setBounds(370,40,50,30);
			sub.addActionListener(subHandler);
			mul.setBounds(300,80,50,30);
			mul.addActionListener(mulHandler);
			div.setBounds(370,80,50,30);
			div.addActionListener(divHandler);
			deriv.setBounds(440,40,100,30);
			deriv.addActionListener(derivHandler);
			integr.setBounds(440,80,100,30);
			integr.addActionListener(integrHandler);
			
			
			con.add(deriv);
			con.add(integr);
			con.add(add);
			con.add(sub);
			con.add(mul);
			con.add(div);
			window.setVisible(true);
		}
	
	

	public static void main(String[] args) {
		new GUI();
	}
	
	public class AddButtonHandler implements ActionListener {
		
		public void actionPerformed(ActionEvent e) {
	
			p1 = new Polinom(_Field1.getText());
			p2 = new Polinom(_Field2.getText());
			
			p1.addPolinoame(p2);
			String s = "";
			
			for(int i = 0; i < p1.rezultat.size(); i++)
			{
				if(p1.rezultat.get(i).getPowerInt() == 0)
					s += p1.rezultat.get(i).getCoeficient();
				else if(p1.rezultat.get(i).getCoeficient() == "0")
					s += "";
				else
				s += p1.rezultat.get(i).getMonom();
			}
			_Result.setText(s);
		}
		
	}

	public class SubButtonHandler implements ActionListener {
		
		public void actionPerformed(ActionEvent e) {
			p1 = new Polinom(_Field1.getText());
			p2 = new Polinom(_Field2.getText());
			//
			
			p1.subPolinoame(p2);
			String s = "";
			
			for(int i = 0; i < p1.rezultat.size(); i++)
			{
				s += p1.rezultat.get(i).getMonom();
			}
			_Result.setText(s);
		}
	}
	
	public class DerivButtonHandler implements ActionListener {
		
		public void actionPerformed(ActionEvent e) {
			p1 = new Polinom(_Field1.getText());
			
			p1.derivPolinoame(p1);
			String s = "";
			
			for(int i = 0; i < p1.rezultat.size(); i++)
			{
				//System.out.println(p3.rezultat.get(0));
				//System.out.println(p1.rezultat.get(0));
				s += p1.rezultat.get(i).getMonom();
			}
			_Result.setText(s);
		}
	}

	public class IntegrButtonHandler implements ActionListener {
		
		public void actionPerformed(ActionEvent e) {
			p1 = new Polinom(_Field1.getText());
			
			p1.integrPolinoame(p1);
			String s = "";
			
			for(int i = 0;i < p1.rezultat.size();i++)
			{
				s += p1.rezultat.get(i).getMonom();
			}
			_Result.setText(s);
		}
	}
	
	public class MulButtonHandler implements ActionListener {
		
		public void actionPerformed(ActionEvent e) {
			p1 = new Polinom(_Field1.getText());
			p2 = new Polinom(_Field2.getText());
			
			p1.mulPolinoame(p2);
			String s = "";
			
			for (int i = 0; i < p1.rezultat.size(); i++)
				s += p1.rezultat.get(i).getMonom();
			
			_Result.setText(s);
		}

	}
	
	public class DivButtonHandler implements ActionListener {
		
		public void actionPerformed(ActionEvent e) {
			p1 = new Polinom(_Field1.getText());
			p2 = new Polinom(_Field2.getText());
			
			p1.divPolinoame(p2);
			String s  = "";
			for(int i = 0 ; i < p1.rezultat.size(); i++)
				s += p1.rezultat.get(i).getMonom();
		}
	}
}
