package PT2018.demo.DemoProject;

import junit.framework.TestCase;

public class Test extends TestCase {

		public void test() {
			
			Polinom p1 = new Polinom("x+4");//2x+4+x+4 = 3x+7
			Polinom p2 = new Polinom("2x+3");
			
			p1.addPolinoame(p2);
			
			String add = "";
			String sub = "";
			
			for (int i = 0; i < p1.rezultat.size(); i++)
				add += p1.rezultat.get(i).getMonom();
			
			p1.subPolinoame(p2);
			
			for (int i = 0; i < p1.rezultat.size(); i++)
				sub += p1.rezultat.get(i).getMonom();
			
			System.out.println(add);
			System.out.println(sub);
			
			Polinom rezultat = new Polinom("+3x+7");
			Polinom rezultat_sub = new Polinom("-1x+1");
			
			assertEquals("ok","+3x+7",add);
			assertEquals("ok","-1x+1",sub);

			
			
			
			
		}
}
