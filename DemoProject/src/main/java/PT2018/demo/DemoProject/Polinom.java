package PT2018.demo.DemoProject;

import java.util.*;

public class Polinom {

		ArrayList<Monom> monoms = new ArrayList<Monom>();
		//ArrayList<Monom> monomsRez = new ArrayList<Monom>();
		ArrayList<Monom> tmp = new ArrayList<Monom>();//??
		ArrayList<Monom> rezultat = new ArrayList<Monom>();//??
		ArrayList<Monom> analiza = new ArrayList<Monom>();
		String s = "";
		Monom monom = new Monom();
		
		public void popAnaliza(ArrayList<Monom> p) {
			
			for(int i = 0; i< p.size(); i++) {
				analiza.add(p.get(i));
			}
			
		}
		/*
		public void fuseDuplicates(int op)
		{
			int i = 0, j = 0;
			boolean parcurs = false;
				whilee:
				while(!parcurs)
				{
					int temp = (int)tmp.get(i).getCoeficientInt();
					
					while(tmp.get(i).getPowerInt() == tmp.get(++j).getPowerInt())
					{
						if(op == 0)
						temp += tmp.get(j).getCoeficientInt();
						else
						temp -= tmp.get(j).getCoeficientInt();
						//if(j == tmp.size()-1)
						//{
						//	rezultat.add(new Monom(temp + "x^" + tmp.get(i).getPower()));
						//	break whilee;
						//}
					}
					rezultat.add(new Monom(temp + "x^" + tmp.get(i).getPower()));
					i=j;
					if(j == tmp.size()-1)
						break;
				}
			rezultat.add(new Monom(tmp.get(i).getCoeficient() + "x^" + tmp.get(i).getPower()));
		}
		*/
		
		public void fuseDuplicates(Polinom p,int op) {
			int i = 0, j = 0;
			rezultat.clear();
			for(int k = 0; k < monoms.size(); k++)
				System.out.println(monoms.get(k).getMonom());

			System.out.println("p.monom");
			for(int k = 0; k < p.monoms.size(); k++)
				System.out.println(p.monoms.get(k).getMonom());
			
			System.out.println("rezultat");
			for(int k = 0; k < rezultat.size(); k++)
				System.out.println(rezultat.get(k).getMonom());
			rezultat.clear();
			
			
			whileloop:		
			while(true) {
				
					if(monoms.get(i).getPowerInt() < p.monoms.get(j).getPowerInt())
					{
						if(op == 1)
						p.monoms.set(j,p.monoms.get(j).mulMonom(new Monom("-1")));
						rezultat.add(p.monoms.get(j));
						j++;
					}
					else if((monoms.get(i).getPowerInt() > p.monoms.get(j).getPowerInt()))
					{
						rezultat.add(monoms.get(i));
						i++;
					}
					else if((monoms.get(i).getPowerInt() == p.monoms.get(j).getPowerInt()))
					{
						if(op == 1)
						rezultat.add(rezultat.size(),monoms.get(i).subMonom(p.monoms.get(j)));
						else
						rezultat.add(rezultat.size(),monoms.get(i).addMonom(p.monoms.get(j)));
						i++;
						j++;
					}
					
					if(i == monoms.size() && j != p.monoms.size())
						i = monoms.size() - 1;
					else if(i != monoms.size() && j == p.monoms.size())
						j = p.monoms.size() - 1;
					else if(i == monoms.size() && j == p.monoms.size())
						break whileloop;
			}
		}
		
		public void addPolinoame(Polinom p) {
			rezultat.clear();
			//for(int i = 0; i < monoms.size(); i++)
			//	tmp.add(monoms.get(i));
			//for(int j = 0; j < p.monoms.size(); j++)
			//	tmp.add(p.monoms.get(j));
			
			//Collections.sort(tmp);
			
			fuseDuplicates(p,0);
		}
		
		public void subPolinoame(Polinom p) {		
			rezultat.clear();
			
			//rezultat.clear();
			for(int i = 0; i < monoms.size(); i++)
				tmp.add(monoms.get(i));
			for(int j = 0; j < p.monoms.size(); j++)
				{
				//p.monoms.set(j,p.monoms.get(j).mulMonom(new Monom("1")));//*-1 si se aduna
				tmp.add(p.monoms.get(j));
				}
			
			//Collections.sort(tmp);
			
			fuseDuplicates(p,1);
			
			
		}
		
		public void derivPolinoame(Polinom p) {
			rezultat.clear();
			for(int i = 0; i < p.monoms.size(); i++)
			{
				System.out.println(monoms.get(i).getMonom());
				rezultat.add(p.monoms.get(i).derivareMonom(p.monoms.get(i)));
				System.out.println(rezultat.get(i).getMonom());
			}
		}
		
		
		public void integrPolinoame(Polinom p) {
			rezultat.clear();
			for(int i = 0; i < p.monoms.size(); i++)
			{
				rezultat.add(monom.integrareMonom(p.monoms.get(i)));
			}
		}
		
		
		public void mulPolinoame(Polinom p) {
			for(int i = 0 ;i < monoms.size(); i++)
			{
				for(int j = 0 ; j < p.monoms.size(); j++)
				{
					rezultat.add(monoms.get(i).mulMonom(p.monoms.get(j)));
				}
			}
			/*
			boolean parcurs = false;
			int i= 0,j = 0;
			whilee:
				while(!parcurs)
				{
					int temp = (int)rezultat.get(i).getCoeficientInt();
					
					while(rezultat.get(i).getPowerInt() == rezultat.get(++j).getPowerInt())
					{
						temp += rezultat.get(j).getCoeficientInt();
						System.out.println(temp +"");
						if(j == rezultat.size()-1)
						{
							tmp.add(new Monom(temp + "x^" + rezultat.get(j).getPower()));
							break whilee;
						}
					}
					tmp.add(new Monom(temp + "x^" + rezultat.get(i).getPower()));
					i=j;
					if(i == rezultat.size())
						break whilee;
					
				}
			for (i = 0; i < tmp.size(); i++) {
				System.out.println(tmp.get(i).getMonom());
			}
			
			
			/*
			monoms.clear();
			p.monoms.clear();
			for(int i = 0; i < rezultat.size(); i++)
				monoms.add(i,rezultat.get(i).mulMonom(new Monom("-1")));
			for(int i = 0 ; i < rezultat.size(); i++) {
				p.monoms.add(i,rezultat.get(i).mulMonom(new Monom("2")));
				//p.monoms.add(rezultat.get(i));
				
			}
			for(int i = 0; i < rezultat.size(); i++)
				System.out.println(rezultat.get(i).getMonom());
			
			System.out.println("------------------------------------------");
			rezultat.clear();
			
			for(int k = 0; k < monoms.size(); k++)
				System.out.println(monoms.get(k).getMonom());

			System.out.println("p.monom");
			for(int k = 0; k < p.monoms.size(); k++)
				System.out.println(p.monoms.get(k).getMonom());
			
			System.out.println("fuse duplicates:");
			
			fuseDuplicates(p);*/

		}
		
		
		public void divPolinoame(Polinom p) {//N/A
			
		}
		

	public Polinom(String a) {
		
		//String s = "4x^3-4x+5x^4-10x^2-234";
		s = a;
		String test = "";
		
		for(char i : s.toCharArray()) {
			
			if(i == '+' && test != "") {
				
				monoms.add(new Monom(test));
				test = "";
			}
			
			if (i == '-' && test != "") {
				
				monoms.add(new Monom(test));
				test = "";
			}
			
			if(i == ' ')
				test = test;
			else
			test += i;
			
			}//for loop
		
		monoms.add(new Monom(test));//adaugare ultimul element
		
		//testprint
		//for(int i = 0; i < monoms.size(); i++)
		//	System.out.println(monoms.get(i).getMonom());

		
		Collections.sort(monoms);//ordonare descrescatoare a monoamelor in ArrayList
		
		//System.out.println("Dupa sortare");
		
		//s = "";//stergere polinom initial
		//for(int i = 0; i < monoms.size(); i++) 
		//{
		//	System.out.println(monoms.get(i).getMonom());
		//	s += monoms.get(i).getMonom();
		//}
		
		//System.out.println("-------------------------------------------");
		
		
		//Monom m0 = new Monom();
		//Monom m1 = new Monom("7");
	//	Monom m2 = new Monom("3");//-6//putere 8
		//System.out.println(m1.getMonom());
		//Monom m2 = new Monom("400x^3");
		//Monom m3 = m1.subMonom(m1);//working + add
		//Monom m3 = m1.derivareMonom(m1);//working
		//rezultat.add(m1.derivareMonom(m1));
		
		//Monom m3 = new Monom();
		//Monom m3 = m0.integrareMonom(m1);
		//m3 = m3.integrareMonom(m3);//working
	/*-------------------------------------------------------------------------------------*/
		//Monom m1 = new Monom("9x^2");
		//Monom m2 = new Monom("3x");
		//rezultat.add(m2.mulMonom(m1));
		//System.out.println(rezultat.get(0).getMonom());
		//Monom m3 = new Monom();
		//m3 = m1.addMonom(m2);
		
		//System.out.println(m3.getMonom());
		//System.out.println(rezultat.get(0).getMonom());
		
		//cu un for rezultatele se pun intr-un monomsRez si se sorteaza descrescator
		
		//Monom m3 = m1.mulMonom(m2);//working
		//Monom m3 = m1.divMonom(m1);//wip
		
		//System.out.println(m3.getMonom());
		
		//System.out.println(Double.parseDouble("2")/Integer.parseInt("5"));
		
		
		
		
		
	}
	
	public Polinom() {}
	
	public static void main(String[] args) {
		//new Polinom("x");
		}//main 
	
}


