package PT2018.demo.DemoProject;

import java.util.*;

public class Monom implements Comparable {
	
	private String monomPart; //aici e retinut monomul care urmeaza sa fie analizat
	private String power = "";//int
	private String coeficient = "";//coeficientul se retine cu tot cu semn///int
	private int powerInt;
	private float coeficientInt;
	private char sign;
	String tmp_Power, tmp_Coeficient = null;

	public Monom() {}
	
	public Monom(String a) {
		
		if(a.charAt(0) != '+' && a.charAt(0) != '-')
				monomPart = "+" + a;
		else
		monomPart = a;
		
		getDate();
	}

	public float getCoeficientInt() {
		return coeficientInt;
	}
	public int getPowerInt() {
		return powerInt;
	}
	public void setCoeficient(int a) {
		this.coeficientInt = a;
	}
	public void setPower(int a) {
		this.powerInt = a;
	}
	public String getMonom() {
		return monomPart;
	}
	
	public String getCoeficient() {
		return coeficient;
	}
	
	public Character getSign() {
		return sign;
	}
	
	public String getPower() {
		return power;
	}
	
	public void getDate() {
		
		sign = monomPart.charAt(0);//set sign
		
		for(int i = 0 ; i < monomPart.length() ; i++) {//set coeficient
			
			if(monomPart.charAt(i) == 'x') {
				coeficientInt = 1;
				break;
			}
			
			if(monomPart.charAt(1) == 'x' && sign == '+')
				coeficient = "1";
			else if(monomPart.charAt(1) == 'x' && sign == '-')
				coeficient = "-1";
			
			else	
			coeficient += monomPart.charAt(i);
			
		}//end for coeficient
		
		coeficientInt = Float.parseFloat(coeficient);
		
		for(int i = 1; i < monomPart.length(); i++) {//set power
			
			if(monomPart.charAt(i) == 'x' && i == monomPart.length()-1)
				power = "1";
			else if(monomPart.charAt(i) == '^') 
			{
				power = ""; //reset power in caz ca s-a citit mai devreme din else
				
				while(i != monomPart.length()-1) 
				{
					i++;
					power += monomPart.charAt(i);
				}
				break;
			}
			else
				power = "0";
			
		}//end for power
		powerInt = Integer.parseInt(power);
		
	}
	
	public Monom addMonom(Monom e) {
		
		String tmp_Power, tmp_Coeficient = null;

		if(Integer.parseInt(this.power) > Integer.parseInt(e.getPower()) )
		{
			tmp_Power = this.power;
			tmp_Coeficient = this.coeficient;
		}
		else
		{
			tmp_Power = e.getPower();
			tmp_Coeficient = e.getCoeficient();
		}
		
		if (Integer.parseInt(this.power) == Integer.parseInt(e.getPower()))
		{
			tmp_Coeficient = String.valueOf(Integer.parseInt(this.coeficient) + Integer.parseInt(e.getCoeficient()));
			tmp_Power = this.power;
		}

		if(tmp_Power == "0")
			return new Monom(tmp_Coeficient);
		else if(tmp_Power == "1")
			return new Monom(tmp_Coeficient + "x");
		else
			return new Monom(tmp_Coeficient + "x^" + tmp_Power);
		//trebuie conditii la afisare pentru 0--sa nu mai afiseze// -1/1 -- sa nu il afiseze
		
	}
	
	public Monom subMonom(Monom e) {
		
		String tmp_Power, tmp_Coeficient = null;

		if(Integer.parseInt(this.power) > Integer.parseInt(e.getPower()) )
		{
			tmp_Power = this.power;
			tmp_Coeficient = this.coeficient;
		}
		else
		{
			tmp_Power = e.getPower();
			tmp_Coeficient = e.getCoeficient();
		}
		
		if (Integer.parseInt(this.power) == Integer.parseInt(e.getPower()))
		{
			tmp_Coeficient = String.valueOf(Integer.parseInt(this.coeficient) - Integer.parseInt(e.getCoeficient()));
			tmp_Power = this.power;
		}
		
		if(tmp_Power == "0")
			return new Monom(tmp_Coeficient);
		else if (tmp_Power == "1")
			return new Monom(tmp_Coeficient + "x");
		else
		return new Monom(tmp_Coeficient + "x^" + tmp_Power);
		
		//trebuie conditii la afisare pentru 0--sa nu mai afiseze// -1/1 -- sa nu il afiseze
		

	}
	
	public Monom derivareMonom(Monom e) {
		
		String tmp_Power, tmp_Coeficient = null;
		
		if(power == "0")
			return new Monom("0");
		else if(power == "1")
			return new Monom(e.coeficient);

		tmp_Coeficient = String.valueOf(Integer.parseInt(this.coeficient) * Integer.parseInt(this.power));
		tmp_Power = String.valueOf(Integer.parseInt(this.power) - 1);
		
		if(tmp_Power == "1")
			return new Monom (tmp_Coeficient + "x");
		else
			return new Monom(tmp_Coeficient + "x^" + tmp_Power);
	}
	
	public Monom  integrareMonom(Monom e) {
		
		
		if(power == "0")
			return new Monom(this.coeficientInt + "x");
		else 
			return new Monom(String.valueOf(e.coeficientInt/(e.powerInt + 1)) + "x^" + String.valueOf(e.powerInt + 1) );
	}
	
	public Monom mulMonom(Monom e) {//pt cazul cu ambii putere 0-----
		
		tmp_Coeficient = String.valueOf(Integer.parseInt(this.coeficient) * Integer.parseInt(e.coeficient));
		tmp_Power = String.valueOf(Integer.parseInt(this.power) + Integer.parseInt(e.power));
		
		Monom tmp;
		
		if(tmp_Power == "0")
		 return	tmp = new Monom(tmp_Coeficient);
		else
		 return	tmp = new Monom(tmp_Coeficient + "x^" + tmp_Power);

	}
	
	public Monom divMonom(Monom e) {//hard
		
		
		return e;
	}

	
	public static void main(String[] args) {}

	//override compareTo pentru a compara valoarea convertita la int a puterii, respectiv a le ordona crecator

	public int compareTo(Object arg0) {
		int comparePower = Integer.parseInt(((Monom)arg0).getPower());//ia val intreaga a puterii
		return comparePower - Integer.parseInt(this.power);
	}
}
